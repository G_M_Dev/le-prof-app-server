const express = require('express')
const sharp = require('sharp')
const { User, UserAllwdUpdates } = require('../models/user')
const Log = require('../models/log')
const { authAdmin, 
        adminIsActive, 
        checkUserSignUpReq, 
        authUser, 
        userIsStudent,
        userIsActive, 
        verifyUserConfirmToken
} = require('../middleware/auth')
const { sendConfirmationEmail, sendReceipt } = require('../emails/account')
const multer = require('multer')
const avtrUpload = multer({
    limits: {
        fileSize: 2000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|JPG|png|PNG|bmp|BMP|jpeg|JPEG)$/)) {
            return cb(new Error('A jpg, png, bmp or jpeg image is expected'))
        }

        cb(undefined, true)
    }
})

const router = new express.Router()

/************ Users REST ***********/
/***********************************/
//--- create a new user (when a user subcribes)
// todo: I have to add CAPTCHA for participants subscription (on client side)...
router.post('/users', checkUserSignUpReq, async (req, res) => {
    const user = new User(req.body)
    user.active = false;
    try {
        await user.save()
        const token = await user.generateAuthToken()
        if (user.role === 'Participant')
            sendConfirmationEmail(
                user.email, 
                `${user.firstName} ${user.lastName}`, 
                `${process.env.SRVR_ROOT_URL}/users/confirm/${token}`
            )
        await user.populate('courses').execPopulate()
        res.status(201).send({ user, token })
    } catch (e) {
        res.status(400).send(e.message)
    }
})
//--- Active user avatar upload
router.post('/users/me/avatar', authUser, userIsActive, avtrUpload.single('actv-usr-avtr'), 
    async (req, res) => {
        const buffer = await sharp(req.file.buffer).resize({width:250, height:250}).png().toBuffer()
        req.user.avatar = buffer
        await req.user.save()
        res.send()
    },
    (error, req, res, next) => {
        res.status(400).send({error: error.message})
    }
)
//--- user logs in
router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        await user.populate('courses').execPopulate()
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (e) {
        res.status(400).send(e.message)
    }
})

//--- user logs out
router.post('/users/logout', authUser, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => token.token !== req.token)
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})
//--- user logs out of all sessions
router.post('/users/logoutAll', authUser, async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

//--- active admin activates a teacher
router.post('/users/activate/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const userToActivate = await User.findOne({_id: req.params.id, role: 'Teacher'})
        if (!userToActivate) return res.status(404).send()
        userToActivate.active = true;
        await userToActivate.save()
        res.send(userToActivate)
    } catch (e) {
        res.status(500).send()
    }
})
//--- Participant activates himself using a special url 
// (the url should be sent by email upon signup)
router.post('/users/confirm/:confirmToken', verifyUserConfirmToken, userIsStudent, 
    async (req, res) => {
        try {
            req.user.active = true
            await req.user.save()
            await Log.createLog(req.user)
            res.send('Your account is activated.')
        } catch (e) {
            res.status(500).send()
        }
})

//--- active admin adds a paid amount to a user
router.post('/users/:id/:paid', authAdmin, adminIsActive, async (req, res) => {
    try {
        const user = await User.findById(req.params.id).populate('courses')
        if (!user) return res.status(404).send()
        if (user.role === 'Participant') user.balance += Number.parseFloat(req.params.paid)
        await user.save()
        await Log.savePayment(user, req.params.paid)
        sendReceipt(
            user.email,
            `${user.firstName} ${user.lastName}`,
            user.birthday.toJSON().substr(0, 10),
            user._id,
            req.params.paid,
            user.balance
        )
        res.send(user)
    } catch (e) {
        res.status(500).send()
    }
})

//--- active admin gets all users
router.get('/users', authAdmin, adminIsActive, async (req, res) => {
    try {
        const users = await User.find({}).populate('courses')
        res.send(users)
    } catch (e) {
        res.status(500).send()
    }
})
//--- user sees his own profile
router.get('/users/me', authUser, async (req, res) => {
    try{
        await req.user.populate('courses').execPopulate()
        res.send(req.user)
    } catch (e) {
        res.status(500).send()
    }
})
//--- get the user avatar
router.get('/users/:id/avatar', async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
        if (!user || !user.avatar) throw new Error()
        res.set('Content-Type', 'image/png')
        res.send(user.avatar)
    } catch (e) {
        res.status(404).send()
    }
})
//--- active admin checks a specific user profile
router.get('/users/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const user = await User.findById(req.params.id).populate('courses')
        if (!user) return res.status(404).send()
        res.send(user)
    }
    catch (e) {
        res.status(500).send()
    }
})
//--- Active participant sees his own balance
router.get('/users/me/balance', authUser, userIsStudent, userIsActive, async (req, res) => {
    try {
        const log = await Log.findOne({participantId: req.user._id})
        const receipt = log.coursesHistory.map(
            (crs) => {
                const {courseTitle, numSessions, plan, price, cost, subscribedDate} = crs
                return {courseTitle, numSessions, plan, price, cost, subscribedDate}
            }
        )
        res.send({
            name: `${req.user.firstName} ${req.user.lastName}`,
            balance: req.user.balance,
            paid: log.paid,
            receipt
        })
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin gets a user balance
router.get('/users/:id/balance', authAdmin, adminIsActive, async (req, res) => {
    try {
        const log = await Log.findOne({participantId: req.params.id})
        const receipt = log.coursesHistory.map(
            (crs) => {
                const {courseTitle, numSessions, plan, price, cost, subscribedDate} = crs
                return {courseTitle, numSessions, plan, price, cost, subscribedDate}
            }
        )
        res.send({
            name: `${log.firstName} ${log.lastName}`,
            balance: log.balance(),
            paid: log.paid,
            receipt
        })
    } catch (e) {
        res.status(500).send()
    }
})

//--- user updating his own profile
router.patch('/users/me', authUser, async (req, res) => {
    const updates = Object.keys(req.body);
    const isValidOp = updates.every((update) => UserAllwdUpdates.includes(update));
    if (!isValidOp) return res.status(400).send('Error: Invalid updates!')
    try {
        updates.forEach((update) =>  req.user[update] = req.body[update])
        await req.user.save()
        await req.user.populate('courses').execPopulate()
        res.send(req.user)
    } catch(e) {
        res.status(400).send(e.message)
    }
})
//--- Active Admin updating a specific user (important to reset a user's password)
router.patch('/users/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const user = await User.findById(req.params.id).populate('courses')
        if (!user) return res.status(404).send()
        const updates = Object.keys(req.body);
        const isValidOp = updates.every((update) => UserAllwdUpdates.includes(update));
        if (!isValidOp) return res.status(400).send('Error: Invalid updates!')
        updates.forEach((update) =>  user[update] = req.body[update])
        await user.save()
        res.send(user)
    } catch(e) {
        res.status(400).send(e.message)
    }
})

//--- user deletes his own profile
router.delete('/users/me', authUser, async (req, res) => {
    try {
        if (!await req.user.canBeDeleted()) {
            return res.status(403).send(req.user.deleteFailMsg())
        }
        await req.user.remove()
        await Log.logDeletion(req.user, req.user)
        res.send(req.user);
    } catch (e) {
        res.status(500).send();
    }
})
//--- Active admin deleting a specific user
router.delete('/users/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
        if (!user) return res.status(404).send();
        if (!await user.canBeDeleted()) {
            return res.status(403).send(user.deleteFailMsg())
        }
        await user.remove()
        await Log.logDeletion(user, req.admin)
        res.send(user);
    } catch (e) {
        res.status(500).send();
    }
})
//--- Active user deletes his own avatar
router.delete('/users/me/avatar', authUser, userIsActive, async (req, res) => {
    req.user.avatar = undefined
    await req.user.save()
    res.send()
})

module.exports = router
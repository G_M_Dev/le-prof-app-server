const express = require('express')
const { Admin, AdminAllwdUpdates } = require('../models/admin')
const {checkSecret, authAdmin, adminIsActive} = require('../middleware/auth')

const router = new express.Router()

/*********** Admins REST ***********/
/***********************************/
//--- create a new admin (when a new Admin subscribes, inactive by default)
router.post('/admins', checkSecret, async (req, res) => {
    const admin = new Admin(req.body)
    admin.active = false; // force active to false upon creation
    try {
        await admin.save()
        const token = await admin.generateAuthToken()
        res.status(201).send({admin, token})
    } catch(e) {
        res.status(400).send('Something went wrong. Please contact another admin to get help.')
    }
})
//--- admin logs in
router.post('/admins/login', async (req, res) => {
    try {
        const admin = await Admin.findByCredentials(req.body.email, req.body.password)
        const token = await admin.generateAuthToken()
        res.send({ admin, token })
    } catch (e) {
        res.status(400).send(e.message)
    }
})

//--- admin logs out
router.post('/admins/logout', authAdmin, async (req, res) => {
    try {
        req.admin.tokens = req.admin.tokens.filter((token) => token.token !== req.token)
        await req.admin.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})
//--- admin logs out of all sessions
router.post('/admins/logoutAll', authAdmin, async (req, res) => {
    try {
        req.admin.tokens = []
        await req.admin.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

//--- active admin activates another
router.post('/admins/activate/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const adminToActivate = await Admin.findById(req.params.id)
        if (!adminToActivate) return res.status(404).send()
        adminToActivate.active = true;
        await adminToActivate.save()
        res.send(adminToActivate)
    } catch (e) {
        res.status(500).send()
    }
}) 

//--- active admin gets all admins
router.get('/admins', authAdmin, adminIsActive, async (req, res) => {
    try {
        const admins = await Admin.find({})
        res.send(admins)
    } catch(e) {
        res.status(500).send()
    }
})
//--- admin sees his own profile
router.get('/admins/me', authAdmin, async (req, res) => {
    res.send(req.admin)
})
//--- active admin checks a specific admin profile
router.get('/admins/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const admin = await Admin.findById(req.params.id)
        if(!admin) res.status(404).send()
        res.send(admin)
    } catch(e) {
        res.status(500).send()
    }
})
//--- admin updates his own profile
router.patch('/admins/me', authAdmin, async (req, res) => {
    const updates = Object.keys(req.body)
    const isValidOp = updates.every((key) => AdminAllwdUpdates.includes(key))
    if (!isValidOp) return res.status(400).send('Error: Invalid updates!')
    try {
        updates.forEach((key) => req.admin[key] = req.body[key])
        await req.admin.save()
        res.send(req.admin)
    } catch(e) {
        res.status(400).send(e.message)
    }
})
//--- admin deletes his own profile
router.delete('/admins/me', authAdmin, async (req, res) => {
    try {
        if(!await req.admin.canBeDeleted()) {
            return res.status(403).send('Impossible to delete the last active admin profile')
        }
        await req.admin.remove()
        res.send(req.admin)
    }
    catch (e) {
        res.status(500).send(e.message)
    }
})
//--- Active Admins deletes inactive admin
router.delete('/admins/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const admin = await Admin.findById(req.params.id);
        if (!admin) return res.status(404).send();
        if (admin.active === true) return res.status(403).send('This admin is active and cannot be deleted by you!')
        await admin.remove()
        res.send(admin);
    } catch (e) {
        res.status(500).send(e.message);
    }
})

module.exports = router
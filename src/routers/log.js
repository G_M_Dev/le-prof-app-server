const express = require('express')
const Log = require('../models/log')
const {User} = require('../models/user')
const { authAdmin, adminIsActive } = require('../middleware/auth')

const router = new express.Router()

/********** Logs REST *************/
/**********************************/

//--- Active admin gets all logs
// GET /logs?deleted=false&subscribedBefore=2015-01-01&updatedAfter=2018-01-01
// GET /logs?limit=5&page=1
// GET /tasks?sortBy=updatedAt_asc[desc]/subscriptionDate_asc[desc]
router.get('/logs', authAdmin, adminIsActive, async (req, res) => {
    const match = {}
    const sort = {}
    if(req.query.deleted !== undefined) {
        match.deletionDate = req.query.deleted==='true'? {$ne: undefined} : {$eq: undefined}
    }
    if (req.query.subscribedBefore !== undefined) {
        match.subscriptionDate = { $lt: new Date(req.query.subscribedBefore) }
    }
    if (req.query.updatedAfter !== undefined) {
        match.updatedAt = { $gt: new Date(req.query.updatedAfter) }
    }
    if (req.query.sortBy) {
        const parts = req.query.sortBy.split('_')
        sort[parts[0]] = parts[1] === 'desc'? -1 : 1
    }
    try {
        const logs = await Log.find(
            match, 
            null,
            {
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.limit)*parseInt(req.query.page),
                sort
            }
        )
        res.send(logs)
    } catch(e) {
        res.status(500).send()
    }
})
//--- Active admin gets a specific user log
router.get('/logs/:userid', authAdmin, adminIsActive, async (req, res) => {
    try {
        const log = await Log.findOne({ participantId: req.params.userid })
        if(!log) return res.status(404).send();
        res.send(log)
    } catch (e) {
        res.status(500).send()
    }
})

//--- Active admin deletes a specific user log
router.delete('/logs/:userid', authAdmin, adminIsActive, async (req, res) => {
    try {
        const user = await User.findById(req.params.userid);
        // deletion is not allowed for undeleted users
        if (user) return res.status(400).send('Error: This user still exists. Related log cannot be deleted')
        const log = await Log.findOneAndDelete({ participantId: req.params.userid })
        if (!log) return res.status(404).send();
        res.send(log);
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router
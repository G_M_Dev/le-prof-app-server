const express = require('express')
const { Course, CourseAllwdUpdates } = require('../models/course')
const { User } = require('../models/user')
const Log = require('../models/log')
const {
    auth,
    isActive,
    authAdmin, 
    adminIsActive, 
    authUser, 
    userIsTeacher, 
    userIsStudent, 
    userIsActive
} = require('../middleware/auth')
const sharp = require('sharp')
const multer = require('multer')
const crsPromoUpload = multer({
    limits: {
        fileSize: 5000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|JPG|png|PNG|bmp|BMP|jpeg|JPEG)$/)) {
            return cb(new Error('A jpg, png, bmp or jpeg image is expected'))
        }

        cb(undefined, true)
    }
})

const router = new express.Router()

/********** Courses REST ***********/
/***********************************/
//--- create a new course (Only Active Teachers can do this)
router.post('/courses', authUser, userIsTeacher, userIsActive, async (req, res) => {
    const course = new Course({
        ...req.body,
        teacher: req.user._id
    })
    try {
        await course.save()
        await course.populate('teacher').execPopulate()
        res.status(201).send(course)
    } catch (e) {
        res.status(400).send(e.message)
    }
})
//--- add a promo image to my course (Active Teacher)
router.post('/courses/:id/promo-img', authUser, userIsTeacher, userIsActive, crsPromoUpload.single('promo'), 
    async (req, res) => {
        const course = await Course.findOne({ teacher: req.user._id, _id: req.params.id })
        if (!course) return res.status(404).send()
        const buffer = await sharp(req.file.buffer).resize({width:750, height:500}).png().toBuffer()
        course.courseImage = buffer
        await course.save()
        res.send()
    },
    (error, req, res, next) => {
        res.status(400).send({error: error.message})
    }
)
//--- enroll in an existing course (Only Active Participants can do this)
router.post('/courses/:id/enroll', authUser, userIsStudent, userIsActive, async (req, res) => {
    const enrollInfo = {
        participant: req.user._id,
        plan: req.body.plan
    }
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        const {bCanEnrol, errorMsg, posInWait} = course.CanEnrol(req.user, enrollInfo.plan)
        if (!bCanEnrol) return res.status(400).send(errorMsg);
        if (posInWait > 0)  course.waitingList.push(enrollInfo)
        else                course.participants.push({...enrollInfo, sessionsDone: 0})
        await course.save()
        await course.populate('teacher').execPopulate()
        if (posInWait <= 0) await Log.addCourse(req.user._id, course, req.body.plan)
        res.send({ course: course.toJSONVisibleToStudent(), posInWait })
    } catch (e) {
        res.status(500).send()
    }
})
//--- leave course (Only active participants can do this)
router.post('/courses/:id/leave', authUser, userIsStudent, userIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        const {bCanLeave, msg, bInWaiting} = course.CanLeave(req.user)
        if(!bCanLeave) return res.status(403).send(msg)
        if (!bInWaiting) {
            course.participants = course.participants.filter(
                (participant) => participant.participant.toJSON()!== req.user._id.toJSON()
            )
        } else {
            course.waitingList = course.waitingList.filter(
                (participant) => participant.participant.toJSON()!== req.user._id.toJSON()
            )
        }
        await course.save()
        await course.populate('teacher').execPopulate()
        if(!bInWaiting) await Log.updateLeaveCourse(req.user, course)
        res.send(course.toJSONVisibleToStudent())
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin ends course cycle
router.post('/courses/:id/endcycle', authAdmin, adminIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        // log leaving participants
        for(participant of course.participants) {
            await Log.updateLeaveCourse(participant.participant, course)
         }
        // remove all participants
        course.participants = []
        const waitingLs = course.waitingList;
        course.waitingList = []
        // move enough waitingList members to participants (first on waitingList)
        for(waiting of waitingLs) {
            if (course.capacity > course.participants.length) {
                const {plan, participant} = waiting
                course.participants.push({plan, participant, sessionsDone: 0})
                await Log.addCourse(participant, course, plan)
            }
            else {
                course.waitingList.push(waiting)
            }
        }
        // save course
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin ends course cycle and close course
router.post('/courses/:id/endcycle-then-close', authAdmin, adminIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        // log leaving participants
        for(participant of course.participants) {
            await Log.updateLeaveCourse(participant.participant, course)
         }
        // remove all participants
        course.participants = []
        // close the course
        course.open = false
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin removes a specific student from a course
router.post('/courses/:id/remove/:userid', authAdmin, adminIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        const userToRemove = course.participants.find(
            (participant) => participant.participant.toJSON() === req.params.userid
        )
        if(userToRemove) {
            await Log.updateLeaveCourse(userToRemove.participant, course)
            course.participants = course.participants.filter(
                (participant) => participant.participant.toJSON()!== req.params.userid
            )
        }
        else {
            course.waitingList = course.waitingList.filter(
                (participant) => participant.participant.toJSON()!== req.params.userid
            )
        }
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin/teacher increments a specific course / student sessions
router.post('/courses/:id/inc-sessions/:incval', auth, isActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        if(req.user && (course.teacher.toJSON() !== req.user._id.toJSON())) return res.status(404).send()
        for(participant of course.participants) {
            if ((participant.plan !== 'All Sessions') || (participant.sessionsDone === 0)) {
                const user = await User.findById(participant.participant)
                await user.payCourse(course, participant.plan, Number.parseFloat(req.params.incval))
                await user.save()
            }
            participant.sessionsDone += Number.parseFloat(req.params.incval)
            await Log.incCourseSessions(participant.participant, course, participant.sessionsDone)
        }
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch(e) {
        res.status(500).send()
    }
})
router.post('/courses/:id/inc-sessions/:userid/:incval', auth, isActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        if(req.user && (course.teacher.toJSON() !== req.user._id.toJSON())) return res.status(404).send()
        const userNdx = course.participants.findIndex(
            (participant) => participant.participant.toJSON() === req.params.userid
        )
        if ((course.participants[userNdx].plan !== 'All Sessions') || (course.participants[userNdx].sessionsDone === 0)) {
            const user = await User.findById(course.participants[userNdx].participant)
            await user.payCourse(course, course.participants[userNdx].plan, Number.parseFloat(req.params.incval))
            await user.save()
        }
        course.participants[userNdx].sessionsDone += Number.parseFloat(req.params.incval)
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        await Log.incCourseSessions(
            course.participants[userNdx].participant, 
            course, 
            course.participants[userNdx].sessionsDone
        )
        res.send(course)
    } catch(e) {
        res.status(500).send()
    }
})
//--- Active admin/teacher closes a course
router.post('/courses/:id/close', auth, isActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        if(req.user && (course.teacher.toJSON() !== req.user._id.toJSON())) return res.status(404).send()
        if (course.participants.length > 0) 
            return res.status(403).send('This course cannot be closed: It still has participants.')
        course.open = false;
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course);
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin/teacher opens a course
router.post('/courses/:id/open', auth, isActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        if(req.user && (course.teacher.toJSON() !== req.user._id.toJSON())) return res.status(404).send()
        const waitingLs = course.waitingList;
        course.waitingList = []
        // move enough waitingList members to participants (first on waitingList)
        for(waiting of waitingLs) {
            if (course.capacity > course.participants.length) {
                const {plan, participant} = waiting
                course.participants.push({plan, participant, sessionsDone: 0})
                await Log.addCourse(participant, course, plan)
            }
            else {
                course.waitingList.push(waiting)
            }
        }
        course.open = true;
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course);
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin assigns the course to another teacher
router.post('/courses/:id/assign-to/:teacher', authAdmin, adminIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if (!course) return res.status(404).send()
        const teacher = await User.findOne({_id: req.params.teacher, role: 'Teacher'})
        if (!teacher) return res.status(404).send()
        course.teacher = teacher._id
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course);
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin updates the course price
router.post('/courses/:id/update-price', authAdmin, adminIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if (!course) return res.status(404).send()
        if (course.participants.length > 0) 
            return res.status(403).send('Impossible to change course price now: It still has participants.')
        course.coursePrice = req.body
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course);
    } catch (e) {
        res.status(500).send()
    }
})

//--- get courses info visible to anyone
router.get('/courses', async (req, res) => {
    try {
        const courses = await Course.find({ open: true })
        for(course of courses) { await course.populate('teacher').execPopulate() }
        const publicCourses = courses.map((course) => course.toJSONVisibleToStudent())
        res.send(publicCourses)
    } catch(e) {
        res.status(500).send()
    }
})
router.get('/courses/:id', async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        await course.populate('teacher').execPopulate()
        if(!course || !course.open) return res.status(404).send()
        res.send(course.toJSONVisibleToStudent())
    } catch(e){
        res.status(500).send()
    }
})
//--- get the course promo image
router.get('/courses/:id/promo-img', async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course || !course.open || !course.courseImage) return res.status(404).send()
        res.set('Content-Type', 'image/png')
        res.send(course.courseImage)
    } catch(e){
        res.status(500).send()
    }
})
//--- get my courses (teacher) [sees his own courses with all the details]
router.get('/mycourses', authUser, userIsTeacher, userIsActive, async (req, res) => {
    try {
        const courses = await Course.find({ teacher: req.user._id })
        for(course of courses){
            await course.populate('teacher').execPopulate()
            await course.populate('participants.participant').execPopulate()
            await course.populate('waitingList.participant').execPopulate()
        }
        res.send(courses)
    } catch(e) {
        res.status(500).send()
    }
})
router.get('/mycourses/:id', authUser, userIsTeacher, userIsActive, async (req, res) => {
    try {
        const course = await Course.findOne({ teacher: req.user._id, _id: req.params.id })
        if(!course) return res.status(404).send()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch(e) {
        res.status(500).send()
    }
})
//--- get my courses (student) [returns public course + plan and sessions completed so far]
router.get('/takencourses', authUser, userIsStudent, userIsActive, async (req, res) => {
    try {
        const courses = await Course.find({ 'participants.participant': req.user._id })
        for(course of courses){ await course.populate('teacher').execPopulate() }
        const publicCourses = courses.map(
            (course) => {
                return {course: course.toJSONVisibleToStudent(), info: course.getUserInfo(req.user._id) } 
            }
        )
        res.send(publicCourses)
    } catch (e) {
        res.status(500).send()
    }
})
router.get('/takencourses/:id', authUser, userIsStudent, userIsActive, async (req, res) => {
    try {
        const course = await Course.findOne({'participants.participant': req.user._id, _id: req.params.id})
        if(!course) return res.status(404).send()
        await course.populate('teacher').execPopulate()
        res.send({course: course.toJSONVisibleToStudent(), info: course.getUserInfo(req.user._id)})
    } catch (e) {
        res.status(500).send()
    }
})
//--- get courses (active admin) [sees everything with all the details]
router.get('/allcourses', authAdmin, adminIsActive, async (req, res) => {
    try {
        const courses = await Course.find({})
        for(course of courses){
            await course.populate('teacher').execPopulate()
            await course.populate('participants.participant').execPopulate()
            await course.populate('waitingList.participant').execPopulate()
        }
        res.send(courses)
    } catch(e) {
        res.status(500).send()
    }
})
router.get('/allcourses/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id)
        if(!course) return res.status(404).send()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch(e) {
        res.status(500).send()
    }
})

//--- Active teacher updates his own course
router.patch('/courses/:id', authUser, userIsTeacher, userIsActive, async (req, res) => {
    const updates = Object.keys(req.body);
    const isValidOp = updates.every((update) => CourseAllwdUpdates.includes(update));
    if (!isValidOp) return res.status(400).send('Error: Invalid updates!')
    try {
        const course = await Course.findById(req.params.id)
        if (!course || (course.teacher.toJSON() !== req.user._id.toJSON())) return res.status(404).send()
        updates.forEach((update) => course[update] = req.body[update])
        await course.save()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch(e) {
        res.status(400).send(e.message)
    }
})

//--- Active teacher deletes one of his courses 
router.delete('/mycourses/:id', authUser, userIsTeacher, userIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id);
        if (!course || (course.teacher.toJSON() !== req.user._id.toJSON())) return res.status(404).send();
        if (course.participants.length > 0) 
            return res.status(403).send('This course cannot be deleted: It still has participants.')
        await course.remove()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course)
    } catch (e) {
        res.status(500).send()
    }
})
//--- Active admin deletes course by id 
router.delete('/courses/:id', authAdmin, adminIsActive, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id);
        if (!course) return res.status(404).send();
        if (course.participants.length > 0) 
            return res.status(403).send('This course cannot be deleted: It still has participants.')
        await course.remove()
        await course.populate('teacher').execPopulate()
        await course.populate('participants.participant').execPopulate()
        await course.populate('waitingList.participant').execPopulate()
        res.send(course);
    } catch (e) {
        res.status(500).send();
    }
})

module.exports = router
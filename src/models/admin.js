const mongoose = require('mongoose')
const {
    validator, 
    ValidatePassword, 
    getPublicProfile,
    generateAuthToken, 
    findByCredentials,
    hashPassword
} = require('../db/validation-utils')

const AdminSchema = new mongoose.Schema({
    firstName: {
        required: true,
        type: String,
        trim: true,
    },
    lastName: {
        required: true,
        type: String,
        trim: true,
    },
    secret: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        required: true,
        type: String,
        validate: [validator.isEmail, 'Invalid email address'],
        trim: true,
        lowercase: true,
        unique: true,
    },
    password: {
        required: true,
        type: String,
        validate: ValidatePassword,
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    // (an admin is created inactive and can't do anything before it is accepted by an active admin)
    // The very first one should already exist -active- in the db; manually added)
    active: {
        type: Boolean,
        default: false
    }
})

AdminSchema.methods.canBeDeleted = async function() {
    if ( await Admin.countDocuments({active : true}) <= 1 ) return false;
    return true;
}

AdminSchema.methods.toJSON = getPublicProfile;

AdminSchema.methods.generateAuthToken = generateAuthToken;

AdminSchema.statics.findByCredentials = findByCredentials;

// Hash the plain text password before saving
AdminSchema.pre('save', hashPassword)

const Admin = mongoose.model('Admin', AdminSchema)
const AdminAllwdUpdates = ['email', 'password', 'secret'];

module.exports = {
    Admin,
    AdminAllwdUpdates
}
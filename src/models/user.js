const mongoose = require('mongoose')
const {
    validator, 
    ValidatePassword, 
    getPublicProfile,
    generateAuthToken, 
    findByCredentials,
    hashPassword
} = require('../db/validation-utils')
const Log = require('./log')

const opts = { toObject: {getters: true} }

// User model
const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true,
    },
    lastName: {
        type: String,
        required: true,
        trim: true,
    },
    avatar: Buffer,
    email: {
        type: String,
        required: true,
        validate: [validator.isEmail, 'Invalid email address'],
        trim: true,
        lowercase: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        validate: ValidatePassword,
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    // Teacher active when enabled by an active Admin
    // Participant enabled when email confirmed
    active: {
        type: Boolean,
        default: false,
    },
    role: {
        required: true,
        type: String,
        enum: ['Teacher', 'Participant']
    },
    birthday: { // to check for course min age requirement (not used for teachers)
        type: Date,
        required: function() {
            return this.role === 'Participant';
        },
    },
    balance: {
        required: function() {
            return this.role === 'Participant';
        },
        type: Number,
        default: 0.0,
    }
}, opts)

UserSchema.virtual('courses', {
    ref: 'Course',
    localField: '_id',
    foreignField: function() {
        return (this.role === 'Teacher')? 'teacher' : 'participants.participant'
    }
})

const getPrice = function (course, plan, sessionsCompleted) {
    if(plan === 'Per Session') { 
        return course.coursePrice.options.perSession*sessionsCompleted;
    } else if (plan === 'All Sessions') {
        return course.coursePrice.options.allSessions;
    }
    throw new Error({error: 'Invalid payment plan!'})
}

UserSchema.methods.payCourse = function (course, plan, sessionsDone) {
    this.balance -= getPrice(course, plan, sessionsDone)
}

UserSchema.methods.canBeDeleted = async function() {
    await this.populate('courses').execPopulate()
    if (this.role === 'Participant') {
        // A participant can't delete himself if his balance is not 0.0
        return this.balance >= 0.0
    } else {
        // A teacher can't be deleted before his courses are reassigned to another teacher or deleted.
        if (this.courses.length !== 0) return false;
    }
    return true;
}

UserSchema.methods.deleteFailMsg = function() {
    if(this.role === 'Participant') {
        return 'This participant balance is not 0.'
    } else {
        return 'This teacher still has courses assigned to him.'
    }
}

UserSchema.methods.toJSON = getPublicProfile;

UserSchema.methods.generateAuthToken = generateAuthToken;

UserSchema.statics.findByCredentials = findByCredentials;

// Hash the plain text password before saving
UserSchema.pre('save', hashPassword)

// remove the user from all courses when deleted
UserSchema.pre('remove', async function (next) {
    if (this.role === 'Participant') {
        await this.populate('courses').execPopulate()
        for(course of this.courses) {
            if (course.participants.find((partic) => partic.participant.toJSON() === this._id.toJSON())) {
                course.participants = course.participants.filter((partic) => partic.participant.toJSON() !== this._id.toJSON())
                await Log.updateLeaveCourse(this._id, course)
            } else {
                course.waitingList = course.waitingList.filter((partic) => partic.participant.toJSON() !== this._id.toJSON())
            }
            course.save()
        }
    }
    next()
})

const User = mongoose.model('User', UserSchema)

const UserAllwdUpdates = ['email', 'password'];

module.exports = {
    User,
    UserAllwdUpdates
}
const mongoose = require('mongoose')

const ObjectId = mongoose.Schema.Types.ObjectId;

// Course model
const CourseSchema = new mongoose.Schema({
    courseTitle: {
        type: String,
        required: true,
    },
    courseDescr: {
        type: String,
        required: true,
    },
    teacher: {
        type: ObjectId,
        required: true,
        ref: 'User'
    },
    participants: [{
        participant:{
            type: ObjectId,
            required: true,
            ref: 'User'
        },
        plan:{
            type: String,
            required: true,
            enum: ['Per Session', 'All Sessions']
        },
        sessionsDone:{
            type: Number,
            required: true,
            default: 0,
            min: 0
        }
    }],
    waitingList: [{
        participant:{
            type: ObjectId,
            required: true,
            ref: 'User'
        },
        plan:{
            type: String,
            required: true,
            enum: ['Per Session', 'All Sessions']
        }
    }],
    courseImage: Buffer,
    open: {
        type: Boolean,
        default: true,
    },
    capacity: {
        type: Number,
        min: 1,
        required: true,
        set(value) {
            return Math.round(value);
        }
    },
    waitingListCapacity: {
        type: Number,
        min: 0,
        default: 1,
        set(value) {
            return Math.round(value);
        }
    },
    courseSessionDuration: {
        type: Number,
        default: 1.0
    },
    courseSchedule: {
        type: [Date],
        required: function() {
            return this.open;
        }
    },
    courseRequirements: {
        type: {
            MinAge: { // positive reasonable age value 0-99
                type: Number,
                min: 3,
                max: 99,
                default: 3,
                set(value) {
                    return Math.round(value);
                }
            },
            OtherRequirements: [String]  // ['Capable de lire en Français', 'Capable de se présenter à 8 séances au moins.'] // etc...
        },
        set: (value) => {
            return { MinAge: Math.round(value.MinAge), OtherRequirements: value.OtherRequirements};
        }
    },
    coursePrice: {  // all values should be in CAD 
        required: function() {
            return this.open;
        },
        type: {
            subscbtionMinPayment: {
                type: Number,
                min: 0.0,
            },
            options: {
                perSession: {
                    type: Number,
                    min: 0.0,
                },
                allSessions: {
                    type: Number,
                    min: 0.0,
                }
            }
        }
    }
})

CourseSchema.methods.CanEnrol = function (user, plan) {
    // check the course is open
    if (!this.open) return { 
        bCanEnrol: false,
        errorMsg: 'This course is not open',
        posInWait: 0
    }
    // check the requested plan exists
    const plansAvailable = Object.keys(this.coursePrice.options)
    if ( (plan === 'All Sessions' && !plansAvailable.includes('allSessions')) ||
         (plan === 'Per Session' && !plansAvailable.includes('perSession')) ) return {
        bCanEnrol: false,
        errorMsg: `This course does not support this plan: ${plan}`,
        posInWait: 0
    }
    // check age requirement
    const userAge = Math.floor( (Date.now() - user.birthday)/1000 /31557600)
    if (userAge < this.courseRequirements.MinAge) return {
        bCanEnrol: false,
        errorMsg: `This course requires a minimal age of ${this.courseRequirements.MinAge}`,
        posInWait: 0
    }
    // check user is not already subscribed
    if( (this.participants.findIndex(
        (participant) => participant.participant.toJSON() === user._id.toJSON()
        ) !== -1) ||
        (this.waitingList.findIndex(
        (participant) => participant.participant.toJSON() === user._id.toJSON()
        ) !== -1) ) return {
            bCanEnrol: false,
            errorMsg: 'Already subscribed to this course',
            posInWait: 0
        }
    // check capacity, check waiting list
    const bCourseFull = this.participants.length >= this.capacity;
    const bWaitingFull = this.waitingList.length >= this.waitingListCapacity;
    if (bWaitingFull) return {
        bCanEnrol: false,
        errorMsg: 'This course is full',
        posInWait: 0
    }
    if (!bCourseFull) {
        return {
            bCanEnrol: true,
            errorMsg: 'success',
            posInWait: 0
        }
    } else {
        return {
            bCanEnrol: true,
            errorMsg: 'success',
            posInWait: this.waitingList.length+1
        }
    }
}

CourseSchema.methods.CanLeave = function(user) {
    // a user can only leave when he doesn't have any sessions done
    let bInWaiting = false;
    let courseInfo = this.participants.find((partic) => partic.participant.toJSON() === user._id.toJSON())
    if (!courseInfo) { 
        courseInfo = this.waitingList.find((partic) => partic.participant.toJSON() === user._id.toJSON())
        if (courseInfo) bInWaiting = true 
    }
    if (!courseInfo) return { bCanLeave: false, msg: 'You are not subscribed to this course!', bInWaiting };
    if (courseInfo.sessionsDone && (courseInfo.sessionsDone > 0))
        return { bCanLeave: false, msg: 'You cannot leave a course you already started.', bInWaiting };
    return { bCanLeave: true, msg: 'success', bInWaiting };
}

CourseSchema.methods.toJSONVisibleToStudent = function () {
    const courseObject = this.toJSON()
    delete courseObject.teacher; 
    courseObject.teacherName = `${this.teacher.firstName} ${this.teacher.lastName}`;
    delete courseObject.participants; 
    courseObject.participantsCount = this.participants.length
    delete courseObject.waitingList;
    courseObject.waitingListCount = this.waitingList.length
    return courseObject
}

CourseSchema.methods.getUserInfo = function (userid) {
    const userInfo = this.participants.find(
        (participant) => participant.participant.toJSON() === userid.toJSON()
    )
    const {plan, sessionsDone} = userInfo
    return {plan, sessionsDone}
}

CourseSchema.methods.toJSON = function () {
    const courseObject = this.toObject()
    delete courseObject.courseImage
    return courseObject
}

const Course = mongoose.model('Course', CourseSchema)

const CourseAllwdUpdates = [
    'courseTitle',
    'courseDescr',
    'capacity',
    'waitingListCapacity',
    'courseSessionDuration',
    'courseSchedule',
    'courseRequirements'
]

module.exports = {
    Course,
    CourseAllwdUpdates
}
const mongoose = require('mongoose')

const ObjectId = mongoose.Schema.Types.ObjectId;

// Log model
const LogSchema = new mongoose.Schema({
    participantId: {
        type: ObjectId,
        required: true,
    },
    firstName: {
        type: String,
        required: true,
        trim: true,
    },
    lastName: {
        type: String,
        required: true,
        trim: true,
    },
    birthday: {
        type: Date,
        required: true,
    },
    paid: {
        required: true,
        type: [
            {
                amount: Number,
                paidAt: Date
            }
        ]
    },
    coursesHistory: {
        required: true,
        type: [
            {
                courseId: ObjectId,
                courseTitle: String,
                numSessions: Number,
                plan: String,
                price: Number,
                cost: Number,
                leaveDate: Date,
                subscribedDate: Date,
                lastUpdate: Date
            }
        ]
    },
    subscriptionDate: {
        type: Date,
        required: true,
    },
    deletionDate: Date,
    deletedBy: {
        deleterId: ObjectId,
        deleterName: String
    }
}, {timestamps: true})

LogSchema.methods.balance = function() {
    const sum = (total, currentAmount) => total+currentAmount;
    const totalPaid = (this.paid.length > 0)? 
        this.paid.map((info) => info.amount).reduce(sum) : 0.0
    const totalCost = (this.coursesHistory.length > 0)? 
        this.coursesHistory.map((crs) => crs.cost).reduce(sum) : 0.0
    return totalPaid - totalCost
}

LogSchema.statics.createLog = async function(user) {
    const logInfo = {
        participantId: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        birthday: user.birthday,
        paid: [],
        coursesHistory: [],
        subscriptionDate: Date.now(),
        deletionDate: undefined,
        deletedBy: {
            deleterId: undefined,
            deleterName: undefined
        }
    }
    const log = new Log(logInfo)
    await log.save()
}

LogSchema.statics.savePayment = async function(user, paid) {
    const log = await Log.findOne({participantId: user._id})
    if (!log) return;
    log.paid.push({
        amount: paid,
        paidAt: Date.now()
    })
    await log.save()
}

LogSchema.statics.logDeletion = async function (user, deleter) {
    const log = await Log.findOne({participantId: user._id})
    if (!log) return;
    log.deletedBy.deleterId = deleter._id
    log.deletedBy.deleterName = `${deleter.firstName} ${deleter.lastName}`
    log.deletionDate = Date.now()
    await log.save()
}

LogSchema.statics.addCourse = async function (userid, course, plan) {
    const log = await Log.findOne({participantId: userid})
    if (!log) return;
    log.coursesHistory.push({
        courseId: course._id,
        courseTitle: course.courseTitle,
        numSessions: 0,
        plan: plan,
        price: (
            plan==='All Sessions'? 
            course.coursePrice.options.allSessions :
            course.coursePrice.options.perSession
        ),
        cost: 0,
        leaveDate: undefined,
        subscribedDate: Date.now(),
        lastUpdate: Date.now()
    })
    await log.save()
}

LogSchema.statics.updateLeaveCourse = async function (userid, course) {
    const log = await Log.findOne({participantId: userid})
    if (!log) return;
    let n = log.coursesHistory.length-1
    for(; n >=0; n--) {
        if (log.coursesHistory[n].courseId.toJSON() === course._id.toJSON()) break;
    }
    if (n === -1) return;
    log.coursesHistory[n].leaveDate = Date.now()
    log.coursesHistory[n].lastUpdate = Date.now()
    await log.save()
}

LogSchema.statics.incCourseSessions = async function (userid, course, sessionsDone) {
    const log = await Log.findOne({participantId: userid})
    if (!log) return;
    let n = log.coursesHistory.length-1
    for(; n >=0; n--) {
        if (log.coursesHistory[n].courseId.toJSON() === course._id.toJSON()) break;
    }
    if (n === -1) return;
    const {plan, price} = log.coursesHistory[n]
    log.coursesHistory[n].numSessions = sessionsDone
    log.coursesHistory[n].cost = (plan==='All Sessions')? price : sessionsDone*price
    log.coursesHistory[n].lastUpdate = Date.now()
    await log.save()
}

const Log = mongoose.model('Log', LogSchema);

module.exports = Log
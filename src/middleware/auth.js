const jwt = require('jsonwebtoken')
const {Admin} = require('../models/admin')
const {User} = require('../models/user')

const checkAdminToken = async (req) => {
    const token = req.header('Authorization').replace('Bearer ', '')
    const decoded = jwt.verify(token, process.env.JWT_SECRET)
    const admin = await Admin.findOne({ _id: decoded._id, 'tokens.token': token })
    return { admin, token }
}

const checkUserToken = async (req) => {
    const token = req.header('Authorization').replace('Bearer ', '')
    const decoded = jwt.verify(token, process.env.JWT_SECRET)
    const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })
    return { user, token }
}

const checkSecret = async (req, res, next) => {
    try {
        if(!await Admin.exists({ secret: req.body.authSecret })) {
            throw new Error()
        }
        next()
    } catch(e) {
        res.status(400).send('Something went wrong. Please contact an admin to get help.')
    }
}

const authAdmin = async (req, res, next) => {
    try {
        const { admin, token } = await checkAdminToken(req)
        if (!admin) throw new Error()
        req.token = token
        req.admin = admin
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate.'})
    }
}

const adminIsActive = async (req, res, next) => {
    try {
        if (!req.admin.active) throw new Error()
        next()
    } catch(e) {
        res.status(404).send()
    }
}

const checkUserSignUpReq = async (req, res, next) => {
    if(req.body.role === 'Teacher') checkSecret(req, res, next)
    else next() // when this is a participant; he should have come through CAPTCHA on client side
}

const authUser = async (req, res, next) => {
    try {
        const { user, token } = await checkUserToken(req)
        if (!user) throw new Error()
        req.token = token
        req.user = user
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate.'})
    }
}

const userIsTeacher = async (req, res, next) => {
    try {
        if (req.user.role !== 'Teacher') throw new Error()
        next()
    } catch(e) {
        res.status(400).send()
    }
}

const userIsStudent = async (req, res, next) => {
    try {
        if (req.user.role !== 'Participant') throw new Error()
        next()
    } catch(e) {
        res.status(400).send()
    }
}

const userIsActive = async (req, res, next) => {
    try {
        if (!req.user.active) throw new Error()
        next()
    } catch(e) {
        res.status(403).send('This account has to be activated.')
    }
}

const verifyUserConfirmToken = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.params.confirmToken, process.env.JWT_SECRET)
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': req.params.confirmToken })
        if (!user) throw new Error()
        user.tokens = user.tokens.filter((token) => token.token !== req.params.confirmToken)
        await user.save()
        req.user = user
        next()
    } catch (e) {
        res.status(400).send()
    }
}

const auth = async (req, res, next) => {
    try {
        const { admin, token } = await checkAdminToken(req)
        if (!admin) {
            const { user, token } = await checkUserToken(req)
            if (!user || (user.role !== 'Teacher')) throw new Error()
            req.token = token
            req.user = user
        }
        else {
            req.token = token
            req.admin = admin
        }
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate.'})
    }
}

const isActive = async (req, res, next) => {
    try {
        if ((req.user && !req.user.active) || (req.admin && !req.admin.active))  throw new Error()
        next()
    } catch(e) {
        res.status(403).send('This account has to be activated.')
    }
}

module.exports = {
    auth,
    isActive,
    checkSecret,
    authAdmin,
    adminIsActive,
    checkUserSignUpReq,
    authUser,
    userIsTeacher,
    userIsStudent,
    userIsActive,
    verifyUserConfirmToken
}
const express = require('express')
require('./db/mongoose')
const adminRouter = require('./routers/admin')
const userRouter = require('./routers/user')
const courseRouter = require('./routers/course')
const logRouter = require('./routers/log')

const app = express()
const port = process.env.PORT

// app.use((req, res, next) => {
//     res.status(503).send('The website is in maintenance...')
// })

app.use(express.json())

app.use(userRouter)
app.use(adminRouter)
app.use(courseRouter)
app.use(logRouter)

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})
const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SG_API_KEY)

const sendConfirmationEmail = (email, name, confirmUrl) => {
    sgMail.send({
        to: email,
        from: process.env.SERVER_EMAIL,
        subject: 'Bienvenue sur LeProf! Confirme ton email',
        html: //todo: style this email (inline css)
        `<h3>Bienvenue sur Le Prof, ${name}!</h3>`+
        '<p>En cliquant sur le lien suivant, vous confirmez votre courriel et vous activez votre compte.</p>'+
        `<form action="${confirmUrl}" method="post">`+
            '<input type="submit" name="confirmation" value="Je confirme mon email"/>'+
        '</form>'
    })
}

const sendReceipt = (email, name, birthday, userid, paidSum, balance) => {
    const t = Date.now()
    sgMail.send({
        to: email,
        from: process.env.SERVER_EMAIL,
        subject: 'Your receipt',
        html: //todo: style this email (inline css)
        '<h2>Confirmation de paiement</h2>'+
        `<h3>${name} (${birthday}) - ID: ${userid.toJSON()}</h3>`+
        `<p>Ce reçu confirme le paiement de la somme de: ${paidSum} CAN.</p>`+
        `<p>Votre solde actuel est de: ${balance}.</p>`+
        `<p>Enregistré au: ${new Date(t).toString()}.</p>`
    })
}

module.exports = {
    sendConfirmationEmail,
    sendReceipt
}
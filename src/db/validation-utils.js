const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const ValidatePassword = (val) => {
    let bValid = true;
    let errorMsg = 'Invalid password: Passwords must contain at least:\n';
    if(!validator.matches(val, /^(?=.{8,})/)) {
        throw new Error('Invalid password: Passwords must contain at least 8 characters.')
    }
    if(!validator.matches(val, /^(?=.*[a-z])/)) {
        errorMsg += '- 1 lowercase alphabetical character\n';
        bValid = false;
    }
    if(!validator.matches(val, /^(?=.*[A-Z])/)) {
        errorMsg += '- 1 uppercase alphabetical character\n';
        bValid = false;
    }
    if(!validator.matches(val, /^(?=.*[0-9])/)) {
        errorMsg += '- 1 numeric character\n';
        bValid = false;
    }
    if(!validator.matches(val, /^(?=.*[!@#\$%\^&\*])/)) {
        errorMsg += '- 1 special character\n';
        bValid = false;
    }
    if (!bValid) { throw new Error(errorMsg.substr(0, errorMsg.length-1)); }
}

const getPublicProfile = function () {
    const userObject = this.toObject()
    delete userObject.password
    delete userObject.tokens
    delete userObject.avatar
    if (userObject.courses){
        userObject.courses.forEach(element => {
            delete element.courseImage
        });
    }
    return userObject
}

const generateAuthToken = async function(expiresIn) { 
    const token = jwt.sign(
        { _id: this._id.toString() }, 
        process.env.JWT_SECRET,
        {expiresIn: (!expiresIn? process.env.TOKEN_LIFE : expiresIn)}
    );
    this.tokens = this.tokens.concat({token})
    await this.save()
    return token
}

const findByCredentials = async function(email, password) {
    const user = await this.findOne({ email })
    if (!user) throw new Error('Unable to login.')
    const isMatch = await bcrypt.compare(password, user.password)
    if (!isMatch) throw new Error('Unable to login')
    return user
}

const hashPassword = async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8)
    }
    next()
}

module.exports = {
    validator,
    ValidatePassword,
    getPublicProfile,
    generateAuthToken,
    findByCredentials,
    hashPassword
}